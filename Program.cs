﻿using System;
using System.Collections.Generic;

namespace exercise_27
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            double totalPrice = 0;

            var items = new Items(10, 10.00);
            var checkout = new Checkout();

            string d;
            
            do
            {
                Console.WriteLine("please type in a number as quantity");
                int _quantity = int. Parse(Console.ReadLine());
                Console.WriteLine("please type in a number as the price");
                double _price = double.Parse(Console.ReadLine());

               totalPrice = totalPrice +(_price * _quantity) ;
                
                checkout.purchase(_quantity,_price);

                Console.WriteLine("Do u want to make another purchase(y for yes ,n for no)");
                d = Console.ReadLine();

            }while(d == "y");
            
            Console.WriteLine ($"The total price is ------------{totalPrice}");
            
            foreach (var item in checkout.Items)
            {
                int q = 1;
                Console.WriteLine ($"~~~{q}");
                Console.WriteLine ();
                Console.WriteLine ($"Quantity-------------------{item.Quantity}"); 
                Console.WriteLine ($"Price----------------------{item.Price}");
                q++;
            }


            checkout.Calculation(totalPrice);

            Console.WriteLine("Thank u for shopping with us:-)");
            
            
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
    class Items
    {
        public int Quantity;
        public double Price;

        public Items(int _quantity, double _price)
        {
            Quantity = _quantity;
            Price = _price;

        }

        

    }
    class Checkout

    {
        public List<Items> Items;

        public Checkout()
        {
            Items = new List<Items>();
        }

        public void Calculation(double totalPrice)
        {
            double GTS = 0.15;
            GTS = GTS * totalPrice;
            totalPrice = totalPrice + GTS;
            Console.WriteLine("The GTS is 15% at the moment");
            Console.WriteLine ($"The total price with GTS is------ {totalPrice}");
        }

        public void purchase(int _quantity, double _price)
        {
            Items.Add(new Items(_quantity, _price));
        }






    }
}
